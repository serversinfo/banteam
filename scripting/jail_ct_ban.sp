#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <csgo_colors>
#include <cstrike>				// для CS_RespawnPlayer(client)
#undef REQUIRE_PLUGIN
#include <adminmenu>
#include <hlstatsX_adv>
#include <jail_stats>
#include <basecomm>
#pragma newdecls required

TopMenu hTopMenu;						//Для админки

bool g_bTurnToCT[MAXPLAYERS+1];						// кто хочет за КТ

/*====Для банов====*/
Handle SQL_hDB	=	INVALID_HANDLE;					// База Данных
Handle ckv		=	INVALID_HANDLE;					// причины бана
Handle QAkv		=	INVALID_HANDLE;					// от QA
Handle h_Timer_RoundStart		=	INVALID_HANDLE;	// таймер с начала раунда
int g_iLastTarget	[MAXPLAYERS+1];					// userid last target
int g_iBanTime		[MAXPLAYERS+1];					// time, choosed in menu
int BanTime			[MAXPLAYERS+1];
int g_iApprovedBy	[MAXPLAYERS+1];					// разрешили ли ему зайти на время и кто
int g_iReasonCount;
bool g_bPlayer_swapped[MAXPLAYERS+1];					// переходил ли игрок в начале раунда.
bool g_bPreWarmup;										// если время перед вармапом

/*====Question-Answer====*/
int		g_iRepeats[MAXPLAYERS+1];						// сколько осталось вопросов
int		g_iNowQuestion[MAXPLAYERS+1];					// какой вопрос сейчас у клиента
bool	g_bGoodQA[MAXPLAYERS+1];						// прошел ли игрок тест в этой сессии?
bool	g_bTesmMe[MAXPLAYERS+1];						// игрок проходит тест по собственному желанию
/*=======================*/

char g_sBanAdminName	[MAXPLAYERS+1][MAX_NAME_LENGTH];	// Ник админа который забанил, записывается в момент бана (CallBackBan), берется на сервере и при коннекте (OnClientPostAdminCheck) берется из mySQL
char g_sBanReason		[MAXPLAYERS+1][64];					// Причина бана
char g_sFileOne		[256];
char g_sFileTwo		[256];
char g_sSteam_id		[MAXPLAYERS+1][32];
char g_sBanReasonShort[64][64];
/*===============*/
bool g_bRndStart;										// true первые n секунд раунда

public Plugin myinfo =
{
	name		= "[JAIL] CT Ban (Private)",
	author		= "Darkeneez & ShaRen",
	description	= "",
	version		= "1.1",
	url			= "Servers-Info.Ru"
}

public void OnPluginStart()
{
	CreateAdminMenu();
	ConnectToBan();
	ReloadConfig();
	
	AddCommandListener(Command_JoinTeam, "jointeam");
	HookEvent("round_end",		Event_RoundEnd);
	HookEvent("round_start",	Event_RoundStart);
	HookEvent("round_prestart", Event_RoundPreStart);
	RegConsoleCmd("sm_testme",	cmdTestMe, "Ты можешь проверить себя на правила");
	RegConsoleCmd("sm_guard",	sm_guard, "Зайти за КТ");
	RegConsoleCmd("sm_ct",		sm_guard, "Зайти за КТ");
	
	RegAdminCmd("sm_ctban",		CtBan, ADMFLAG_BAN, "sm_ctban <#userid|name>, <time>, <reason>");
	
	CreateTimer(1.0, Timer_OneSecond, _, TIMER_REPEAT);
}


/*===========================================================
	Конфиг
=============================================================
*/
int ReloadConfig()
{
	ckv = CreateKeyValues("Reasons");
	
	BuildPath(Path_SM, g_sFileOne, sizeof (g_sFileOne), "logs/CtUnBans/UnBan.log");
	BuildPath(Path_SM, g_sFileTwo, sizeof (g_sFileTwo), "configs/teambans.cfg");		//причины бана
	
	if(!FileToKeyValues(ckv, g_sFileTwo))
		SetFailState("Configs not found");
	
	g_iReasonCount = 0;
	
	if(KvJumpToKey(ckv, "BanReasons")) {
		if(KvGotoFirstSubKey(ckv, false)) {
			do {
				KvGetSectionName(ckv, g_sBanReasonShort[g_iReasonCount], sizeof(g_sBanReasonShort));
				KvGetString(ckv, NULL_STRING, g_sBanReasonShort[g_iReasonCount], sizeof(g_sBanReasonShort));
				ReplaceString(g_sBanReasonShort[g_iReasonCount], sizeof(g_sBanReasonShort), "'", "", false);
				g_iReasonCount++;
			} while(KvGotoNextKey(ckv, false) && g_iReasonCount < 64);
		}
	}
	KvRewind(ckv);
	return g_iReasonCount;
}

/*===========================================================
	Question-Answer
=============================================================
*/
public Action cmdTestMe(int client, int args)
{
	if (IsClientInGame(client)) {
		g_iRepeats[client] = 40;
		g_bTesmMe[client] = true;
		CreateMenuQuestionAnswer(client);
	}
	return Plugin_Continue;
}

int GetQuestionsForPlayer(int client)
{
	int time = GetClientPlayedTime(client);
	if (time == -1)			return 10;	// если не определен
	if (time < 1800)		return 32;	// 30 мин
	if (time < 3600)		return 20;	// 1ч
	if (time < 7200)		return 15;	// 2ч
	if (time < 14400)		return 10;	// 4ч
	if (time < 21600)		return 7;	// 6ч
	if (time < 28800)		return 5;	// 8ч
	else					return 0;
}

void ShowAbout(int client, int question)
{
	if (QAkv != INVALID_HANDLE)
		CloseHandle(QAkv);
	QAkv = CreateKeyValues("Question-Answer");
	char sPath[256];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/question-answer.ini");
	if (FileExists(sPath)) {
		if (!FileToKeyValues(QAkv, sPath))
			SetFailState("Can not convert file to KeyValues: %s", sPath);
		KvRewind(QAkv);
		char sQuestion[10];
		IntToString(question, sQuestion, sizeof(sQuestion));
		if(KvJumpToKey(QAkv, sQuestion)) {
			if(KvJumpToKey(QAkv, "about")) {
				char sNumAbout[10];
				char sAbout[1024];
				int i=1;
				while(i < 15) {
					IntToString(i, sNumAbout, sizeof(sNumAbout));
					KvGetString(QAkv, sNumAbout, sAbout, sizeof(sAbout), "none");
					if (StrEqual(sAbout, "none"))
						break;
					CGOPrintToChat(client, "%s", sAbout);
					i++;
				}
			} else LogError("Not found case: about");
		} else LogError("Not found case: %d", 1);
	} else SetFailState("File Not Found: %s", sPath);
}

public Action CreateMenuQuestionAnswer(int client)
{
	if (QAkv != INVALID_HANDLE)
		CloseHandle(QAkv);
	QAkv = CreateKeyValues("Question-Answer");
	char sPath[256];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/question-answer.ini");
	if (FileExists(sPath)) {
		if (!FileToKeyValues(QAkv, sPath))
			SetFailState("Can not convert file to KeyValues: %s", sPath);
		KvRewind(QAkv);
		int MaxQuestion = 0;
		for (bool result = KvGotoFirstSubKey(QAkv); result; result = KvGotoNextKey(QAkv))
			MaxQuestion ++;
		if ( !MaxQuestion )
			SetFailState("Can not find any data in file: %s", sPath);
		KvRewind(QAkv);
		Menu menu = CreateMenu(ChoiceMenu);
		int time;
		int attempt = MaxQuestion;
		while(attempt > 0) {
			int iRandomCase = GetRandomInt(1,MaxQuestion);
			g_iNowQuestion[client] = iRandomCase;
			char sRandomCase[256];
			IntToString(iRandomCase, sRandomCase, sizeof(sRandomCase));
			if(KvJumpToKey(QAkv, sRandomCase)) {
				char sQuestion[256];
				KvGetString(QAkv, "question", sQuestion, sizeof(sQuestion), "none");
				if(!StrEqual(sQuestion, "none")) {
					menu.SetTitle(sQuestion);
					int iCount = KvGetNum(QAkv, "count", 0);
					if(iCount) {
						int iNumTrueAnswer = KvGetNum(QAkv, "true", 0);
						if(iNumTrueAnswer && iNumTrueAnswer<=iCount) {
							char sAnswer[256];
							char sTrueAnswer[256];
							char sNumTrueAnswer[256];
							IntToString(iNumTrueAnswer, sNumTrueAnswer, sizeof(sNumTrueAnswer));
							KvGetString(QAkv, sNumTrueAnswer, sTrueAnswer, sizeof(sTrueAnswer), "none");
							if(!StrEqual(sTrueAnswer, "none")) {
								int iRanTrueSelectItem = GetRandomInt(1,iCount);	// какой № будет верный 
								char sTmpAnswer[256];
								char sRanTrueSelectItem[256];
								IntToString(iRanTrueSelectItem, sRanTrueSelectItem, sizeof(sRanTrueSelectItem));
								KvGetString(QAkv, sRanTrueSelectItem, sTmpAnswer, sizeof(sTmpAnswer), "none");	// что было под новым правильным пунктом
								if(!StrEqual(sTmpAnswer, "none"))
									for(int num = 1; num<=iCount; num++) {
										if(num == iRanTrueSelectItem)		// если этот номер правильный 
											menu.AddItem("true", sTrueAnswer);
										else if(num == iNumTrueAnswer)		// если под этим номер был правльный ответ
											menu.AddItem("false", sTmpAnswer);
										else {	// sTrueAnswer и sTmpAnswer поменяли местами, остальное все по порядку как в конфиге
											char snum[256];
											IntToString(num, snum, sizeof(snum));
											KvGetString(QAkv, snum, sAnswer, sizeof(sAnswer), "none");
											if(!StrEqual(sAnswer, "none"))
												menu.AddItem("false", sAnswer);
											else {
												Error(attempt, true, "File structure is interrupted: Bad section '%d', case: %d", num, iRandomCase);
												break;
											}
										}
									} else Error(attempt, true, "File structure is interrupted: Bad section '%d', case: %d", iRanTrueSelectItem, iRandomCase);
							} else Error(attempt, true, "File structure is interrupted: Bad section '%d', case: %d", iNumTrueAnswer, iRandomCase);
							time = KvGetNum(QAkv, "time", 20);
							attempt = -1;
						} else Error(attempt, true, "File structure is interrupted: Bad section 'true', case: %d", iRandomCase);
					} else Error(attempt, true, "File structure is interrupted: Bad section 'count', case: %d", iRandomCase);
				} else  Error(attempt, true, "File structure is interrupted: Bad section 'question', case: %d", iRandomCase);
			} else Error(attempt, false, "Not found case: %d", iRandomCase);
		}
		if(!attempt)
			SetFailState("File structure is interrupted: Bad ALL section: rewrite File!");
		else if(attempt == -1) {
			menu.ExitButton = true;
			if(g_bTesmMe[client])
				menu.Display(client, 100);
			else menu.Display(client, time);
		}
	} else SetFailState("File Not Found: %s", sPath);
}

void Error(int attempt, bool bGoBack, const char[] cMess, any:...)
{
	LogError(cMess);
	attempt--;
	if(bGoBack)
		KvGoBack(QAkv);
}

public int ChoiceMenu(Menu menu, MenuAction action, int client, int choice)
{
	if(client > 0 && IsClientInGame(client)) {
		if (action == MenuAction_Select) {
			char info[32];
			menu.GetItem(choice, info, sizeof(info));
			if(StrEqual(info, "true")) {
				if(g_iRepeats[client]) {
					g_iRepeats[client]--;
					CGOPrintToChat(client, "{LIGHTBLUE}Верно, едем дальше!");
					CreateMenuQuestionAnswer(client);
				} else {
					CGOPrintToChat(client, "{BLUE}Все верно!");
					g_bGoodQA[client] = true;
					if (!g_bTesmMe[client])
						FakeClientCommand(client, "jointeam 3");
				}
			} else {
				CGOPrintToChat(client, "{RED}Ответ не правильный!");
				ShowAbout(client, g_iNowQuestion[client]);
				CGOPrintToChat(client, "Ссылка на правила: {BLUE}http://goo.gl/RnJ9P0 {DEFAULT} Пиши {BLUE}!testme{DEFAULT} чтобы тренероваться");
				if (GetClientPlayedTime(client) > 30000)
					CGOPrintToChat(client, "Нашли ошибку, хотите добавить вопрос? skype: {BLUE}sharteman{DEFAULT}");
				else CGOPrintToChat(client, "Для вас будет показано {RED}%i {DEFAULT}вопросов. Чем больше вы играете на сервере, тем меньше вопросов.", GetQuestionsForPlayer(client));
				if (g_bTesmMe[client])
					CreateMenuQuestionAnswer(client);
			}
		} else if (action == MenuAction_Cancel)
			CGOPrintToChat(client, "{RED}Вы не ответили");
	} else if (action == MenuAction_End)
		delete menu;
}

/*===========================================================
	Игрок
===========================================================
*/
public void OnClientPostAdminCheck(int client)
{
	if (client > 0 && !IsClientSourceTV(client)) {
		GetClientAuthId(client, AuthId_Steam2, g_sSteam_id[client], sizeof(g_sSteam_id));
		SQL_LoadPlayer(client);
	}
	g_bTurnToCT[client] = false;
}

public void OnClientDisconnect(int client)
{	// чтобы счетчик не сбрасывался
	g_bTurnToCT[client] = false;
	if(IsPlayerBanned(client))
		SQL_SavePlayer(client);
	if(g_iApprovedBy[client])
		g_iApprovedBy[client] = 0;
	for(int i=1; i<=MaxClients; i++)
		if (g_iApprovedBy[i] == client)
			g_iApprovedBy[i] = 0;
}


public Action sm_guard(int client, int args)
{
	FakeClientCommand(client, "jointeam 3");
	CGOPrintToChat(client, "{GREEN}Писать !guard не обязательно, можно просто выбрать команду КТ");
}

public void OnMapStart()
{
	HookEvent("player_team", 	Event_PlayerTeam);
	g_bPreWarmup = true;
}


public void Event_PlayerTeam(Event event, const char[] name, bool dontBroadcast)
{
	int iTeam = event.GetInt("team");
	if (iTeam == 2 || iTeam == 3) {
		g_bPreWarmup = false;
		UnhookEvent("player_team", 	Event_PlayerTeam);
	}
}

public Action Command_JoinTeam(int client, const char[] command, int args)
{
	if(!IsValidPlayer(client))
		return Plugin_Continue;
		
	char strTeam[8];
	GetCmdArg(1, strTeam, sizeof(strTeam));
	int newTeam = StringToInt(strTeam);
	int oldTeam = GetClientTeam(client);
	
	int Ts = GetTeamClientCount(2);
	int Cs = GetTeamClientCount(3);
	// если 1й раз за сессию выбираешь команду
	if (!oldTeam) {
		ClientCommand(client, "firstperson;");		// если на другом сервере игроку был установлен вид от 3 лица.
		g_bGoodQA[client] = false;
	}
	if(newTeam == 3) {
		if (g_bTurnToCT[client]) {
			g_bTurnToCT[client] = false;
			CGOPrintToChat(client, "{RED}Вы вышли из очереди за КТ!");
			PrintHintText(client, "Вы вышли из очереди за КТ!");
		} else if (BaseComm_IsClientMuted(client)) {
			CGOPrintToChat(client, "{LIME}С мутом заходить за КТ запрещено!");
		} else if (IsPlayerBanned(client) && !g_iApprovedBy[client]) {
			CGOPrintToChat(client, "Забанен админом %s. До разбана нужно наиграть %i мин. Причина: %s.", g_sBanAdminName[client], RoundToCeil(BanTime[client]/60.0), g_sBanReason[client]);
			Handle hPl2 = CreatePanel();
			char buffer[256];
			Format(buffer, sizeof(buffer), "Забанен админом %s\nДо разбана нужно наиграть %i мин.\nПричина: %s\nПравила можете узнать на сайте\nServers-Info.Ru\nИли пиши !testme", g_sBanAdminName[client], RoundToCeil(BanTime[client]/60.0), g_sBanReason[client]);
			DrawPanelText(hPl2, buffer);
			SendPanelToClient(hPl2, client, Handler_DoNothing, 15);
			ClientCommand( client, "play buttons/button11.wav" );
			SwapPlayer(client, 2, oldTeam);
		} else if(!g_bGoodQA[client]) {
			g_iRepeats[client] = GetQuestionsForPlayer(client);
			g_bTesmMe[client] = false;
			CreateMenuQuestionAnswer(client);
			if (!oldTeam)
				ChangeClientTeam(client, 1);
		} else if (GameRules_GetProp("m_bWarmupPeriod") || g_bPreWarmup) {
			g_bTurnToCT[client] = true;
			CGOPrintToChat(client, "{LIME}Во время разминки заходить за КТ запрещено!");
			CGOPrintToChat(client, "{BLUE}Вы добавлены в очередь за КТ!");
			PrintHintText(client, "Вы добавлены в очередь за КТ! Чтобы выйти из очереди выберите команду КТ ещё раз!");
			CGOPrintToChat(client, "{BLUE}Чтобы {RED}выйти {BLUE}из очереди выберите команду КТ ещё раз!");
		} else if (Cs > (Ts-((oldTeam == 2)? 1:0))/3) {
			CGOPrintToChat(client, "{LIME}Слишком много КТ!");
			g_bTurnToCT[client] = true;
			CGOPrintToChat(client, "{BLUE}Вы добавлены в очередь за КТ!");
			PrintHintText(client, "Вы добавлены в очередь за КТ! Чтобы выйти из очереди выберите команду КТ ещё раз!");
			CGOPrintToChat(client, "{BLUE}Чтобы {RED}выйти {BLUE}из очереди выберите команду КТ ещё раз!");
			ClientCommand(client, "play ui/freeze_cam.wav");
			SwapPlayer(client, 2, oldTeam);
		} else if (Cs && !g_bRndStart && IsPlayerAlive(client)) {
			g_bTurnToCT[client] = true;
			CGOPrintToChat(client, "{LIME}Вы будете переведены за КТ в след раунде! (т.к. вы сейчас живы)");
			CGOPrintToChat(client, "{LIME}Если хотите перейти немедленно, то попробуйте ещё раз когда будете мертвы!");
		} else {
			SwapPlayer(client, 3, oldTeam);
			if (g_iApprovedBy[client] && IsClientInGame(g_iApprovedBy[client]))
				CGOPrintToChatAll("{LIME}%N перешел за КТ с разрешения %N!", client, g_iApprovedBy[client]);
		}
		
	} else if (newTeam == 0) 	// автовыбор
		SwapPlayer(client, 2, oldTeam);
	else SwapPlayer(client, newTeam, oldTeam);
	return Plugin_Handled;
}

void SwapPlayer(int client, int iTeam, int oldTeam=-1)
{	// нужно для возрождений в начале раунда
	if (iTeam == oldTeam)
		return;

	bool bAlive = IsPlayerAlive(client);
	if(bAlive)
		StripAllWeapons(client);
	ChangeClientTeam(client, iTeam);
	if(bAlive && g_bRndStart && iTeam > 1 && !g_bPlayer_swapped[client]) {
		g_bPlayer_swapped[client] = true;
		if (IsValidPlayer(client) && !IsPlayerAlive(client))				// используется IsPlayerAlive вместо bAlive т.к. после ChangeClientTeam игрок может умереть, а может нет
			CS_RespawnPlayer(client);
	}
}

public int Handler_DoNothing(Menu menu, MenuAction action, int param1, int param2) {}


public void Event_RoundPreStart(Event event, const char[] name, bool dontBroadcast)
{
	if (GameRules_GetProp("m_bWarmupPeriod") || g_bPreWarmup)
		return;
	for (int i=1; i<=MaxClients; i++)
		if (IsClientInGame(i) && GetClientTeam(i) == 3 && IsPlayerBanned(i) && !g_iApprovedBy[i]) {
			SwapPlayer(i, 2, 3);
			CGOPrintToChat(i, "[CTban]{LIME}Админ временно разрешивший вам играть вышел из игры!");
		}

	int Ts = GetTeamClientCount(2);
	int Cs = GetTeamClientCount(3);
	int iNeed, WorstCT, BestCT;
	for(;;)
		if (float(Cs-iNeed) > (Ts+iNeed)/2.9) {
			WorstCT = GetWorstCT();
			if (WorstCT) {
				SwapPlayer(WorstCT, 2);
				CGOPrintToChat(WorstCT, "[TeamBalance]{LIME}Вы были переведены за Т из-за дисбаланса!");
				CGOPrintToChatAll("[TeamBalance]{LIME}%N был переведен за Т из-за дисбаланса!", WorstCT);
			} else break;
			iNeed++;
		} else break;
	if (!iNeed)			// не пришлось переводить за Т, значит может наоборот нужны КТ
		for(int j=0;;)
			if (Cs+iNeed < (Ts-iNeed+j)/3.0) {			// делаем так чтобы не пересчитывать кол-во Т после каждого перевода
				BestCT = GetBestCT();
				if (BestCT) {
					if(GetClientTeam(BestCT) != 2)
						j++;
					SwapPlayer(BestCT, 3);
					CGOPrintToChat(BestCT, "[TeamBalance]{LIME}Вы были переведены за КТ т.к. были в очереди!");
					CGOPrintToChatAll("[TeamBalance]{LIME}%N был переведен за КТ т.к. был в очереди!", BestCT);
					g_bTurnToCT[BestCT] = false;
				} else {
					int BestCTCond = GetCTCond();
					if (BestCTCond) {
						CGOPrintToChat(BestCTCond, "{RED}ВНИМАНИЕ!!!!");
						CGOPrintToChat(BestCTCond, "{BLUE}Есть место за КТ. Сервер счел вас лучшей кандидатурой");
						CGOPrintToChat(BestCTCond, "{LIME}Перейдите сейчас и вы возродитесь за КТ");
						if (!Cs && Ts>5) {
							CGOPrintToChat(BestCTCond, "{RED}Убедительа просьба зайти за КТ, чтобы остальные игроки не ушли от скуки");
							CGOPrintToChat(BestCTCond, "{RED}Сервер будет вам благодарен за это.");
						}
					}
					break;
				}
				iNeed++;
			} else break;
	for(int iBestPts, iWorstPts;;) {
		WorstCT = GetWorstCT();
		BestCT = GetBestCT();
		if (!WorstCT || !BestCT)
			break;
		iBestPts = GetRank(BestCT, 3);
		iWorstPts = GetRank(WorstCT, 3);
		if (float(iWorstPts) < iBestPts/10) {
			SwapPlayer(WorstCT, 2);
			CGOPrintToChat(WorstCT, "{LIME}В место вас зашел более опытный игрок");
			SwapPlayer(BestCT, 3);
			CGOPrintToChat(BestCT, "{LIME}Вы поменялись с менее опытным игроком");
			CGOPrintToChatAll("{LIME}Вместо %N за КТ зашел %N т.к. у него больше опыта", WorstCT, BestCT);
			g_bTurnToCT[BestCT] = false;
		} else break;
	}
}

int GetWorstCT()
{
	int iMinPoints, iMinCT;
	for(int i=1; i<=MaxClients; i++)
	if(IsClientInGame(i) && GetClientTeam(i) == 3 && iMinPoints > GetRank(i, 3)) {
		iMinPoints = GetRank(i, 3);
		iMinCT = i;
	}
	return iMinCT;
}

int GetBestCT()
{
	int iMaxPoints, iMaxCT;
	for(int i=1; i<=MaxClients; i++)
		if(IsClientInGame(i) && GetClientTeam(i) != 3 && g_bTurnToCT[i] && !g_iApprovedBy[i] && iMaxPoints < GetRank(i, 3)) {
			iMaxPoints = GetRank(i, 3);
			iMaxCT = i;
		}
	return iMaxCT;
}

int GetCTCond()
{
	int iMaxPoints, iMaxCT;
	for(int i=1; i<=MaxClients; i++)
		if(IsClientInGame(i) && GetClientTeam(i) != 3 && !IsPlayerBanned(i) && iMaxPoints < GetRank(i, 3)) {
			iMaxPoints = GetRank(i, 3);
			iMaxCT = i;
		}
	return iMaxCT;
}

public void Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	if (h_Timer_RoundStart != INVALID_HANDLE) {		// Таймер с начала раунда
		KillTimer(h_Timer_RoundStart);
		h_Timer_RoundStart = INVALID_HANDLE;
	}
	h_Timer_RoundStart = CreateTimer(35.0, Timer_RoundStart);			// время с начала раунда когда функцией нельзя ползоваться
	g_bRndStart = true;
	for (int i=1; i<=MaxClients; i++)
		g_bPlayer_swapped[i] = false;
}
public Action Timer_RoundStart(Handle timer)
{
	h_Timer_RoundStart = INVALID_HANDLE;
	//dbgMsg("g_bRndStart to false");
	g_bRndStart = false;									// по истечении n сек с начала раунда
}

public void Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	SQL_SaveALL();
}

public void OnMapEnd ()
{
	SQL_SaveALL();
}

void SQL_SaveALL()
{
	for(int i=1; i<=MaxClients; i++)
		if(!IsValidPlayer(i))
			SQL_SavePlayer(i);
}

public Action Timer_OneSecond(Handle timer)
{
	for(int i=1; i<=MaxClients; i++) {
		if(!IsValidPlayer(i))
			continue;
		
		if(BanTime[i] > 0)
			if((BanTime[i]--) == 0) {			// тут ежесекундно вычитается секунда с бана
				char admin_name[10] = "CONSOLE";
				ResetVariables(i, admin_name);
			}
	}
}
/*===========================================================
	Ф-ции
===========================================================
*/
stock bool IsValidPlayer(int client)
{
	return (client>0 && client <= MaxClients && IsClientInGame(client) && !IsFakeClient(client)) ? true : false;
}

stock bool IsPlayerBanned(int client)
{
	return BanTime[client] ?  true : false;
}

stock void ResetVariables(int client, const char[] admin_name)		// Разбан
{
	BanTime[client]			= 0;
	g_sBanReason[client]		= "UNBANNED";
	Format(g_sBanAdminName[client], MAX_NAME_LENGTH, "%s", admin_name);

	SQL_SavePlayer(client);
}

stock int CheckTime(int time)
{
	if (time != -1)
		time = RoundToCeil(time/60.0);
	return time;
}

stock int GetImmunity(int client) 
{
	AdminId id = view_as<AdminId>(GetUserAdmin(client)); 
	int immun; 
	if (id !=  view_as<AdminId>(-1))
		immun = GetAdminImmunityLevel(id); 
	return immun; 
}
/*===========================================================
	База Данных Банов
===========================================================
*/
void ConnectToBan()
{
	if(SQL_hDB != INVALID_HANDLE) {
		CloseHandle(SQL_hDB);
		SQL_hDB = INVALID_HANDLE;
	}
	if(SQL_CheckConfig("CT-ban"))
		SQL_TConnect(CallBackConnectSQL, "CT-ban");
	else LogError("(ConnectToBan) Unable to connect to database, reason: no config entry found for 'CT-ban' in databases.cfg");
}

public void CallBackConnectSQL(Handle owner, Handle hndl, const char[] error, any data)
{
	if(hndl == INVALID_HANDLE) {
		LogError("(CallBackConnectSQL) Unable to connect to database, reason: %s", error);
		return;
	}
	SQL_hDB = CloneHandle(hndl);
	
	char query[256];
	Format(query, sizeof(query), "CREATE TABLE IF NOT EXISTS bt (steamid VARCHAR(32), time INTEGER(16), admin VARCHAR(32), reason VARCHAR(64))");
	SQL_TQuery(SQL_hDB, CallBackCreateTable, query, DBPrio_High);
}

public void CallBackCreateTable(Handle owner, Handle hndl, const char[] error, any data)
{
	if(hndl == INVALID_HANDLE)
		LogError("Could not create table, reason: %s", error);
}

stock bool BanTeam(int client, int target, int time, const char[] reason)
{
	if(SQL_hDB == INVALID_HANDLE)
		return false;

	if (GetClientOfUserId(g_iLastTarget[client]) != target) {
		if (!client)
			PrintToServer("[SM] Player no longer available");
		else PrintToChat(client, "[SM]  Player no longer available");
		return false;
	}
	
	if(IsValidPlayer(target)) {
		if(time < 0)
			time = -1;
			
		char buffer[256], name[MAX_NAME_LENGTH];

		if(!time) {
			GetClientName(client, name, sizeof(name));
			ReplaceString(name, sizeof(name), "'", "", false);
		}
		
		Format(buffer, sizeof(buffer), "UPDATE bt SET time = '%i', reason = '%s', admin = '%s' WHERE steamid = '%s'", time, reason, name, g_sSteam_id[target]);
		
		DataPack dp = CreateDataPack();
		dp.WriteCell(client);
		dp.WriteCell(target);
		dp.WriteCell(time);
		dp.WriteString(reason);
		
		SQL_TQuery(SQL_hDB, CallBackBan, buffer, dp);
		PrintToConsole(client, "[TB] You banned \"%N\" (%i) for %i seconds for \" %s\"", target, GetClientUserId(target), time, reason);
		g_bTurnToCT[client] = false;
		if (time) {
			time = CheckTime(time);
			LogToFileEx(g_sFileOne, "[%N(%s)] banned [%N (%s)] for %i", client, g_sSteam_id[client], target, g_sSteam_id[target], time);
		}
		return true;
	}
	return false;
}


public void CallBackBan(Handle owner, Handle hndl, const char[] error, DataPack dp)
{
	if(hndl == INVALID_HANDLE) {
		LogError("Unable to ban the player, reason: %s", error);
		return;
	}
	
	dp.Reset();
	int client = dp.ReadCell();										//client
	int target = dp.ReadCell();										//target
	BanTime[target] = dp.ReadCell();								//time
	dp.ReadString(g_sBanReason[target], sizeof(g_sBanReasonShort));	//reason
	
	if(BanTime[target] != 0) {
		char buffer[MAX_NAME_LENGTH];
		GetClientName(client, buffer, MAX_NAME_LENGTH);
		ReplaceString(buffer, sizeof(buffer), "'", "", false);
		strcopy(g_sBanAdminName[target], MAX_NAME_LENGTH, buffer);
		SwapPlayer(target, 2, GetClientTeam(target));
		SQL_SavePlayer(target);
		
		if(BanTime[target] < 0) {
			CGOPrintToChatAll("%s забанил %N навсегда за %s.", g_sBanAdminName[target], target, g_sBanReason[target]);
			PrintHintText(target, "Ты забанен за КТ навсегда, админом %s. %s", g_sBanAdminName[target], g_sBanReason[target]);
		} else {
			int time = RoundToCeil(BanTime[target]/60.0);
			CGOPrintToChatAll("%s забанил %N на %i мин. за %s.", g_sBanAdminName[target], target, time, g_sBanReason[target]);
			PrintHintText(target, "Ты забанен за КТ на %i мин., админом %s. %s", time, g_sBanAdminName[target], g_sBanReason[target]);
		}
	} else {	// значит разбаниваем
		char name_[MAX_NAME_LENGTH];
		GetClientName(client, name_, MAX_NAME_LENGTH);
		ResetVariables(target, name_);
		CGOPrintToChatAll("%N разбанил %N за КТ.", client, target);
	}
	delete dp;
}

stock void StripAllWeapons(int client)
{
	int wepIdx;
	for (int i=0; i<4; i++)
		if ((wepIdx = GetPlayerWeaponSlot(client, i)) != -1) {			// && i!=2 #define CS_SLOT_KNIFE	2	/**< Knife slot. */
			RemovePlayerItem(client, wepIdx);
			AcceptEntityInput(wepIdx, "Kill");
		}
}

void SQL_LoadPlayer(int client)
{
	char query[256];

	Format(query, sizeof(query), "SELECT time, admin, reason FROM bt WHERE steamid = '%s'", g_sSteam_id[client]);
	if	(SQL_hDB != INVALID_HANDLE)
		SQL_TQuery(SQL_hDB, CallBackLoadPlayer, query, client, DBPrio_Normal);
}

public void CallBackLoadPlayer(Handle owner, Handle hndl, const char[] error, any client)
{
	if(hndl == INVALID_HANDLE) {
		LogError("Unable to load the player, reason: %s", error);
		return;
	}
	
	if(!SQL_FetchRow(hndl))
		SQL_InsertPlayer(client);
	else {
		BanTime[client] = SQL_FetchInt(hndl, 0);
		SQL_FetchString(hndl, 1, g_sBanAdminName[client], MAX_NAME_LENGTH);
		SQL_FetchString(hndl, 2, g_sBanReason[client], sizeof(g_sBanReasonShort));
	}
}

void SQL_InsertPlayer(int client)
{
	char query[256];
	
	Format(query, sizeof(query), "INSERT INTO bt (steamid, time, admin, reason) VALUES ('%s', 0, ' ', ' ')", g_sSteam_id[client]);
	SQL_TQuery(SQL_hDB, CallBackInsertPlayer, query, DBPrio_Normal);
}

public void CallBackInsertPlayer(Handle owner, Handle hndl, const char[] error, any client)
{
	if(hndl == INVALID_HANDLE)
		LogError("Unable to create the player, reason: %s", error);
}

void SQL_SavePlayer(int target)
{
	char query[512];
	Format(query, sizeof(query), "UPDATE bt SET time = '%d', admin = '%s', reason = '%s' WHERE steamid = '%s'", BanTime[target], g_sBanAdminName[target], g_sBanReason[target], g_sSteam_id[target]);
	SQL_TQuery(SQL_hDB, CallBackSavePlayer, query, target, DBPrio_Normal);
}

public void CallBackSavePlayer(Handle owner, Handle hndl, const char[] error, int anydata)
{
	if(hndl == INVALID_HANDLE)
		LogError("Unable to update the player, reason: %s", error);
}

/*===========================================================
	Добавляю в админку
===========================================================
*/

/*Проверяю библиотеку*/
void CreateAdminMenu()
{
	TopMenu topmenu;
	if(LibraryExists("adminmenu") && ((topmenu = GetAdminTopMenu()) != null))
		OnAdminMenuReady(topmenu);
}

public void OnLibraryRemoved(const char[] name)
{
	if(StrEqual(name, "adminmenu"))
		hTopMenu = null;
}

/*Добавляю пункт в меню*/
public void OnAdminMenuReady(Handle aTopMenu)
{
	TopMenu topmenu = TopMenu.FromHandle(aTopMenu);
	if(topmenu == hTopMenu)
		return;
		
	hTopMenu = topmenu;		// Save the Handle
	
	TopMenuObject player_commands = hTopMenu.FindCategory(ADMINMENU_PLAYERCOMMANDS);		// Find the "Player Commands" category
	if(player_commands != INVALID_TOPMENUOBJECT)
		hTopMenu.AddItem("sm_ctban", AdminMenu_ChoosePlayer, player_commands, "sm_ctban", ADMFLAG_BAN);
}

/*Админ выбрал игрока*/
public void AdminMenu_ChoosePlayer(TopMenu topmenu, TopMenuAction action, TopMenuObject object_id, int client, char[] buffer, int maxlength)
{
	//g_IsWaitingForChatReason[param] = false;		// причину в чат
	if(action == TopMenuAction_DisplayOption)
		Format(buffer, maxlength, "КТ-бан");					//Перевод
	else if(action == TopMenuAction_SelectOption)
		//DisplayMenu(BuildMenuChoosePlayer(client), client, MENU_TIME_FOREVER);		//Вызываем и создаем МЕНЮ
		DisplayBanTargetMenu(client);
}

void DisplayBanTargetMenu(int client)
{
	Menu menu = new Menu(MenuHandler_BanPlayerList);

	char title[100];
	Format(title, sizeof(title), "Выбери игрока:");
	menu.SetTitle(title);
	menu.ExitBackButton = true;

	char display[64], userid[8];
	for(int i=1; i<=MaxClients; i++) {
		if(!IsValidPlayer(i) || GetClientTeam(i) !=3 || GetImmunity(client) < GetImmunity(i) )		// первыми в списке будут КТ
			continue;
		PrintToChat(client, "%i %i %i", client, i,  GetUserFlagBits(client) & ADMFLAG_CHANGEMAP);
		if(client == i && GetUserFlagBits(client) & ADMFLAG_CHANGEMAP == 0)
			continue;
		Format(display, sizeof(display), "%s%N (%i)", IsPlayerBanned(i)?"[*] ":"", i, GetClientUserId(i));
		IntToString(GetClientUserId(i), userid, sizeof(userid));
		AddMenuItem(menu, userid, display);
	}
	for(int i=1; i<=MaxClients; i++) {
		if(!IsValidPlayer(i) || GetClientTeam(i) ==3 || GetImmunity(client) < GetImmunity(i) )		// пойдут в списке после КТ
			continue;
		PrintToChat(client, "%i %i %i", client, i,  GetUserFlagBits(client) & ADMFLAG_CHANGEMAP);
		if(client == i && GetUserFlagBits(client) & ADMFLAG_CHANGEMAP == 0)
			continue;
		Format(display, sizeof(display), "%s%N (%i)", IsPlayerBanned(i)?"[*] ":"", i, GetClientUserId(i));
		IntToString(GetClientUserId(i), userid, sizeof(userid));
		AddMenuItem(menu, userid, display);
	}

	menu.Display(client, MENU_TIME_FOREVER);
}

public Action CtBan(int client, int args)
{
	if(!client)
		ReplyToCommand(client, "[SM] Usage: sm_ctban <#userid|name> Only in game throw menu usage");
	if (args < 1) {
		DisplayMenu(BuildMenuChoosePlayer(client), client, MENU_TIME_FOREVER);
		ReplyToCommand(client, "[SM] Usage: sm_ctban <#userid|name>");
		return Plugin_Handled;
	}
	char arg[65];
	GetCmdArg(1, arg, sizeof(arg));

	char target_name[MAX_TARGET_LENGTH];
	int target_list[MAXPLAYERS], target_count;
	bool tn_is_ml;

	target_count = ProcessTargetString(
					arg,
					client,
					target_list,
					MaxClients,
					COMMAND_FILTER_CONNECTED|COMMAND_FILTER_NO_MULTI,
					target_name,
					sizeof(target_name),
					tn_is_ml);

	if(target_count <= COMMAND_TARGET_NONE) {		// If we don't have dead players COMMAND_TARGET_NONE	0	/**< No target was found */
		ReplyToTargetError(client, target_count);
		return Plugin_Handled;
	}
	g_iLastTarget[client] = GetClientUserId(target_list[0]);
	if(IsPlayerBanned(target_list[0]))
		DisplayMenu(BuildMenuEditBan(client), client, MENU_TIME_FOREVER);
	else DisplayBanTimeMenu(client);
	return Plugin_Continue;
}

/*Создаем меню*/
stock Handle BuildMenuChoosePlayer(int client)
{
	char display[64], userid[8];
	Menu menu = CreateMenu(HandlerChoosePlayer);
	menu.SetTitle("Выбери игрока:");
	
	for(int i=1; i<=MaxClients; i++) {
		if(!IsValidPlayer(i) || GetClientTeam(i) !=3 || GetImmunity(client) < GetImmunity(i) )		// первыми в списке будут КТ
			continue;
		Format(display, sizeof(display), "%s%N (%i)", IsPlayerBanned(i)?"[*] ":"", i, GetClientUserId(i));
		IntToString(GetClientUserId(i), userid, sizeof(userid));
		menu.AddItem(userid, display);
	}
	for(int i=1; i<=MaxClients; i++) {
		if(!IsValidPlayer(i) || GetClientTeam(i) ==3 || GetImmunity(client) < GetImmunity(i) )		// пойдут в списке после КТ
			continue;
		Format(display, sizeof(display), "%s%N (%i)", IsPlayerBanned(i)?"[*] ":"", i, GetClientUserId(i));
		IntToString(GetClientUserId(i), userid, sizeof(userid));
		menu.AddItem(userid, display);
	}
	menu.ExitBackButton = true;
	menu.ExitButton = true;
	
	return menu;
}

public int HandlerChoosePlayer(Menu menu, MenuAction action, int client, int param2)
{
	if(action == MenuAction_Select) {
		char info[8];
		int target = GetClientOfUserId(StringToInt(info));
		menu.GetItem(param2, info, sizeof(info));
		if(IsValidPlayer(target)) {
			g_iLastTarget[client] = StringToInt(info);
			if(IsPlayerBanned(target))
				DisplayMenu(BuildMenuEditBan(client), client, MENU_TIME_FOREVER);
			else DisplayBanTimeMenu(client);
		} else CGOPrintToChat(client, "Неправильная цель!");
	} else if(action == MenuAction_Cancel) {
		if(param2 == MenuCancel_ExitBack && hTopMenu != INVALID_HANDLE)
			DisplayTopMenu(hTopMenu, client, TopMenuPosition_LastCategory);
	} else if(action == MenuAction_End)
		delete menu;
}

public int MenuHandler_BanPlayerList(Menu menu, MenuAction action, int client, int param2)
{
	if (action == MenuAction_End)
		delete menu;
	else if (action == MenuAction_Cancel && param2 == MenuCancel_ExitBack && hTopMenu)
			hTopMenu.Display(client, TopMenuPosition_LastCategory);
	else if (action == MenuAction_Select) {
		char info[32], name[32];
		menu.GetItem(param2, info, sizeof(info), _, name, sizeof(name));
		int target = GetClientOfUserId(StringToInt(info));

		if (!target)
			PrintToChat(client, "[SM] Игрок больше не доступен.");
		else if (!CanUserTarget(client, target))
			PrintToChat(client, "[SM] Невозможно применить к этому игроку.");
		else if(IsValidPlayer(target)) {
			g_iLastTarget[client] = StringToInt(info);
			if(IsPlayerBanned(target))
				DisplayMenu(BuildMenuEditBan(client), client, MENU_TIME_FOREVER);
			else DisplayBanTimeMenu(client);
			
		}
	}
}

//stock Handle BuildMenuBanPlayer(int client)
//{
//	Handle menu = CreateMenu(HandlerBanPlayer);
//	SetMenuTitle(menu, "%T", "Select time:", LANG_SERVER);
//	
//	int target = GetClientOfUserId(g_iLastTarget[client]);
//	if(IsPlayerBanned(target))
//		AddMenuItem(menu, "0", "Разбанить");
//	else AddMenuItem(menu, "3", "3 Минуты");
//	AddMenuItem(menu, "10", "10 Минут");
//	AddMenuItem(menu, "30", "30 Минут");
//	AddMenuItem(menu, "60", "1 Час");
//	AddMenuItem(menu, "120", "2 Часа");
//	AddMenuItem(menu, "240", "4 Часа");
//	AddMenuItem(menu, "480", "8 Часов");
//	AddMenuItem(menu, "1440", "Сутки");
//	
//	SetMenuExitBackButton(menu, true);
//	SetMenuExitButton(menu, true);
//	
//	return menu;
//}

void DisplayBanTimeMenu(int client)
{
	Menu menu = new Menu(HandlerBanPlayer);

	int target = GetClientOfUserId(g_iLastTarget[client]);
	char title[100];
	Format(title, sizeof(title), "Забанить за КТ %N", target);
	menu.SetTitle(title);
	menu.ExitBackButton = true;

	if(IsPlayerBanned(target)) {
		menu.AddItem("0",	"Разбанить");
		if (!g_iApprovedBy[target]) {
			menu.AddItem("-2",	"Разрешить на время (на эту сессию)");
		} else menu.AddItem("-2",	"Снять разрешение от %N", g_iApprovedBy[target]);
	}
	else menu.AddItem("3",	"3 Минуты");
	menu.AddItem("10",   	"10 Минут");
	menu.AddItem("30",		"30 Минут");
	menu.AddItem("60",		"1 Час");
	menu.AddItem("120",		"2 Часа");
	menu.AddItem("240",		"4 Часа");
	menu.AddItem("480",		"8 Часов");
	menu.AddItem("1440",	"Сутки");
	menu.AddItem("10080",	"Неделя (нет 14)");

	menu.Display(client, MENU_TIME_FOREVER);
}

public int HandlerBanPlayer(Menu menu, MenuAction action, int client, int param2)
{
	if(action == MenuAction_Select) {
		int target = GetClientOfUserId(g_iLastTarget[client]);
		if(IsValidPlayer(target)) {
			char info[32];
			menu.GetItem(param2, info, sizeof(info));
			g_iBanTime[client] = StringToInt(info);
			if(g_iBanTime[client] == 0) {
				BanTeam(client, target, 0, "");
				LogToFileEx(g_sFileOne, "[%N(%s)] has unbanned by [%N (%s)]", target, g_sSteam_id[target], client, g_sSteam_id[client]);
			} else if (g_iBanTime[client] == -2) {
				if (!g_iApprovedBy[target]) {
					g_iApprovedBy[target] = client;
					CGOPrintToChatAll("Админ %N временно разрешил заходить %N за КТ", client, target);
				} else {
					g_iApprovedBy[target] = 0;
					SwapPlayer(target, 2, GetClientTeam(target));
					CGOPrintToChat(target, "%N снял временное разрешение захода за КТ", client);
				}
			} else if (g_iBanTime[client] == 10080) {
				//menu.GetItem(param2, info, sizeof(info));
				//PrintToConsole(client, "%i %i", client, target);
				//PrintToConsole(client, "%N %N", client, target);
				BanTeam(client, target, g_iBanTime[client]*60, "Неделя (нет 14)");
			} else BuildMenuBanPlayerReason(client);
		} else CGOPrintToChat(client, "Непраильная цель!");
	} else if(action == MenuAction_Cancel) {
		if(param2 == MenuCancel_ExitBack && hTopMenu)
			hTopMenu.Display(client, TopMenuPosition_LastCategory);
	} else if(action == MenuAction_End)
		delete menu;
}


public int MenuHandler_BanTimeList(Menu menu, MenuAction action, int client, int param2)
{
	if (action == MenuAction_End)
		delete menu;
	else if (action == MenuAction_Cancel && param2 == MenuCancel_ExitBack && hTopMenu)
		hTopMenu.Display(client, TopMenuPosition_LastCategory);
	else if (action == MenuAction_Select) {
		char info[32];

		menu.GetItem(param2, info, sizeof(info));
		g_iBanTime[client] = StringToInt(info);

		BuildMenuBanPlayerReason(client);
	}
}

stock Handle BuildMenuBanPlayerReason(int client)
{
	Menu menu = new Menu(HandlerBanPlayerReason);
	char title[100];
	Format(title, sizeof(title), "Причина КТ-бана %N", GetClientOfUserId(g_iLastTarget[client]));
	menu.SetTitle(title);
	menu.ExitBackButton = true;
	
	//Add custom chat reason entry first
	//menu.AddItem("", "Custom reason (type in chat)");
	
	char info[8];
	for(int i=0; i<g_iReasonCount; i++) {
		IntToString(i, info, sizeof(info));
		menu.AddItem(info, g_sBanReasonShort[i]);
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int HandlerBanPlayerReason(Menu menu, MenuAction action, int client, int param2)
{
	if(action == MenuAction_Select) {
		int target = GetClientOfUserId(g_iLastTarget[client]);
		if(IsValidPlayer(target)) {
			//if(param2 == 0) {
				//Chat reason
				//g_IsWaitingForChatReason[client] = true;
				//PrintToChat(client, "[SM] %t", "Custom ban reason explanation", "sm_abortban");
			//} else {
				char info[64];
				menu.GetItem(param2, info, sizeof(info));		// id причины
				BanTeam(client, target, g_iBanTime[client]*60, g_sBanReasonShort[StringToInt(info)]);
			//}
		} else CGOPrintToChat(client, "Неправильная цель!");
	} else if(action == MenuAction_Cancel && param2 == MenuCancel_ExitBack && hTopMenu) {
		hTopMenu.Display(client, TopMenuPosition_LastCategory);
	} else if(action == MenuAction_End)
		delete menu;
}

stock Handle BuildMenuEditBan(int client)
{
	char display[128];
	int target = GetClientOfUserId(g_iLastTarget[client]);
	Menu menu = CreateMenu(HandlerEditBan);
	SetMenuTitle(menu, "Отредактировать бан игрока %N", target);
	
	Format(display, sizeof(display), "Забанен за %s", g_sBanReason[target]);
	menu.AddItem("", display, ITEMDRAW_DISABLED);
	
	Format(display, sizeof(display), "Забанил админ %s", g_sBanAdminName[target]);
	menu.AddItem("", display, ITEMDRAW_DISABLED);
	
	if(BanTime[target] < 0)
		Format(display, sizeof(display), "Забанен навсегда");
	else Format(display, sizeof(display), "До разбана %i мин.", RoundToCeil(BanTime[target]/60.0));
	menu.AddItem("", display, ITEMDRAW_DISABLED);
	
	Format(display, sizeof(display), "Перебанить/Разбанить");
	menu.AddItem("ban", display);
	
	menu.ExitBackButton =true;
	menu.ExitButton = true;
	return menu;
}

public int HandlerEditBan(Menu menu, MenuAction action, int client, int param2)
{
	if(action == MenuAction_Select) {
		char info[8];
		menu.GetItem(param2, info, sizeof(info));
		if(StrEqual(info, "ban"))
			DisplayBanTimeMenu(client);
	} else if(action == MenuAction_Cancel) {
		if(param2 == MenuCancel_ExitBack)
			DisplayMenu(BuildMenuChoosePlayer(client), client, MENU_TIME_FOREVER);
	} else if(action == MenuAction_End)
		delete menu;
}